package ru.will0376.stuckstuck;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = Stuckstuck.MOD_ID,
		name = Stuckstuck.MOD_NAME,
		version = Stuckstuck.VERSION)
public class Stuckstuck {

	public static final String MOD_ID = "stuckstuck";
	public static final String MOD_NAME = "Stuckstuck";
	public static final String VERSION = "1.0";

	@Mod.Instance(MOD_ID)
	public static Stuckstuck INSTANCE;

	@Mod.EventHandler
	public void preinit(FMLPreInitializationEvent event) {

	}

	@Mod.EventHandler
	public void init(FMLInitializationEvent event) {

	}

	@Mod.EventHandler
	public void postinit(FMLPostInitializationEvent event) {

	}
}