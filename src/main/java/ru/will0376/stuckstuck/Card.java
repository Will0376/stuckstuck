package ru.will0376.stuckstuck;

import lombok.Data;
import net.minecraft.item.ItemStack;

import java.io.Serializable;

@Data
public class Card implements Serializable {
	private String cardHash;
	private ItemStack stack;
	public }
